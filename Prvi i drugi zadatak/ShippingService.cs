﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prvi_zadatak
{
    class ShippingService
    {
        private double pricePerKilo;

        public ShippingService(double price)
        {
            this.pricePerKilo = price;
        }

        public double Price(double weight)
        {
            return weight * pricePerKilo;
        }

        public override string ToString()
        {
            return "Total price for shipping is: ";
        }

    }
}
