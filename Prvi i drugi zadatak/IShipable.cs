﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prvi_zadatak
{
    interface IShipable
    {
        double Price { get; }
      
        string Description(int depth = 0);
        double Weight { get; }
    }
}
