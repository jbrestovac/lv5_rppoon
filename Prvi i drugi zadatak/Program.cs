﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prvi_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IShipable> products = new List<IShipable>();
            Box box = new Box("something");
            Product firstProduct = new Product("notebook", 5.99, 0.2);
            Product secondProduct = new Product("pencil", 10.5, 0.1);
            products.Add(box);
            products.Add(firstProduct);
            products.Add(secondProduct);


            ShippingService price = new ShippingService(4);
            double weight = 0;
            foreach (IShipable ship in products)
            {

                weight += ship.Weight;

                Console.WriteLine(ship.Description());

            }

            Console.WriteLine(price.ToString() + price.Price(weight) + " hrk");


        }
    }
}
