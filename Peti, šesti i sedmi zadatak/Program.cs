﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Peti_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme firstTheme = new LightTheme();
            ITheme secondTheme = new DarkTheme();


            ReminderNote firstNote = new ReminderNote("hello", firstTheme);
            ReminderNote secondNote = new ReminderNote("world", secondTheme);

            //6.zadatak
            GroupNote thirdNote = new GroupNote("something important", firstTheme);
            thirdNote.InsertNote("faculty");
            thirdNote.InsertNote("i don' t have time to do all this");
            thirdNote.Show();
            thirdNote.RemoveNote(1);
            thirdNote.Show();

            //7.zadatak
            Notebook notes = new Notebook(firstTheme);
            notes.AddNote(firstNote);
            notes.AddNote(secondNote);
            notes.ChangeTheme(secondTheme);
            notes.AddNote(firstNote);
            notes.Display();

        }
    }
}
