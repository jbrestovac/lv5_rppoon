﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Peti_zadatak
{
    //6.zadatak
    class GroupNote : Note
    {

        private List<String> names;

        public GroupNote(string message, ITheme theme) : base(message, theme) { names = new List<String>(); }

        public void InsertNote(String name)
        {

            names.Add(name);


        }
        public void RemoveNote(int name)
        {
            names.RemoveAt(name);


        }
        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("Names: ");
            string framedMessage = this.GetFramedMessage();
            Console.WriteLine(framedMessage);


            foreach (String i in names)
            {

                this.ChangeColor();

                Console.WriteLine(i);

                Console.ResetColor();

            }
        }
    }
}
