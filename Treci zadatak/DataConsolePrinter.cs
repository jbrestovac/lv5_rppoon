﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treci_zadatak
{
    class DataConsolePrinter
    {
        public void Print(IDataset data)
        {
            IReadOnlyCollection<List<string>> Data = data.GetData();
            if (Data == null)
            {
                Console.WriteLine("Empty");
                return;

            }
            foreach(List<string> items in Data)
            {
                foreach(string item in items)
                {
                    Console.WriteLine(item + " ");
                }
            }
            Console.WriteLine();
        }

    }
}
