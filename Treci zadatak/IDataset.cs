﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
namespace Treci_zadatak
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();
    }
}
