﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Treci_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Dataset data = new Dataset("C:\\Users\\Jelena\\source\\repos\\RPPOON-LV5-BRESTOVACJ\\CSVfile.csv");
            User user1 = User.GenerateUser("Jelena");
            User user2 = User.GenerateUser("Jovana");
            ProtectionProxyDataset proxy1 = new ProtectionProxyDataset(user1);
            ProtectionProxyDataset proxy2 = new ProtectionProxyDataset(user2);
            VirtualProxyDataset proxy3 = new VirtualProxyDataset("C:\\Users\\Jelena\\source\\repos\\RPPOON-LV5-BRESTOVACJ\\CSVfile.csv");
            DataConsolePrinter print = new DataConsolePrinter();
            print.Print(proxy1);
            print.Print(proxy2);
            print.Print(proxy3);

         
        
        }
    }
}
